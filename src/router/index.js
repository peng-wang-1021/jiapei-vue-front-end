import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import Home from '../views/Home'
import Permissions from '../views/Permissions.vue'
import Roles from '../views/Roles.vue'
import User from '../views/User.vue'
import Bunk from '../views/Bunk.vue'

import Hospital from '../views/Hospital.vue'
import Department from '../views/Department.vue'



import Order from '../views/Order.vue'
import Orders from '../views/Orders.vue'
import moment from "moment"
import UserDetails from '../views/UserDetails.vue'


import MaintainRecord from '../views/MaintainRecord.vue'
import Malfunction from '../views/Malfunction.vue'

Vue.use(VueRouter)
Vue.prototype.$moment = moment;
const routes = [
  {
     "path": "/Home", component: Home,
     

     children:[
        { "path": "/Home/User", component: User },
        { "path": "/Home/Roles", component: Roles },
        { "path": "/Home/Permissions", component: Permissions },

        { "path": "/Home/Bunk", component: Bunk },


        { "path": "/Home/Hospital", component: Hospital },
        { "path": "/Home/Department", component: Department },

        { "path": "/Home/Order", component: Order },
        { "path": "/Home/Orders/:id", component: Orders },
        { "path": "/Home/UserDetails/:id", component: UserDetails },

        {"path": "/Home/Malfunction",component: Malfunction },
        {"path": "/Home/MaintainRecord",component: MaintainRecord }

     ] 
    },
  { "path": "/Login", component: Login },
 
]

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {
  if (to.path == "/Login") {
    next();
    return;
  }

  let user = sessionStorage.getItem("user");

  if (!user) {
    // 流程能进入这里，说明用户没有登录
    router.push("/Login");
   
    return;
  }

  // 放行
  next();

});
export default router
