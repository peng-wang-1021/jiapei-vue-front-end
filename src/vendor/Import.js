
import XLSX from 'xlsx'

function readExcel(file) {
  const types = file.name.split('.')[1];
  const fileType = [
    'xlsx', 'xlc', 'xlm', 'xls', 'xlt', 'xlw', 'csv'
  ].some(item => item == types);
  if (!fileType) {
    alert('格式错误！请重新选择');
    return
  }
  return new Promise(function (resolve, reject) {
    const reader = new FileReader();
    var result123 = [];
    reader.onload = function (e) {
      const data = e.target.result;
      const wb = XLSX.read(data, {
        type: 'binary'
      });
      wb.SheetNames.forEach((sheetName) => {
        result123.push({
          sheet: XLSX.utils.sheet_to_json(wb.Sheets[sheetName])
        })
      });
    };
    reader.readAsBinaryString(file.raw)

    resolve(result123);
  })
}

export { readExcel }